<<<<<<< HEAD
# 2GO Fullstack dev

Teste para dev fullstack.

- [ ] Fazer o clone deste repositório
- [ ] Crie uma branch com seu nome
- [ ] (opcional) Instalar o NGINX
- [ ] (opcional) Instalar o Redis
- [ ] (opcional) Instalar o PHP7.4 com os módulos fpm, gd, intl, mbstring, bcmath, zip, json, xml, curl, mysql e redis
- [ ] Instalar o Elasticsearch 7+
- [ ] Instalar o Laravel Jetstream + Inertia (VueJS)
- [ ] Criar um vhost my-test.2go.local apontando para sua instalação do Laravel
- [ ] Registrar seu usuário
- [ ] Habilitar o 2FA para o login
- [ ] Criar, no Laravel, uma rotina de console que busque todos os resultados possívies da API do MercadoLivre https://api.mercadolibre.com/sites/MLB/search?q=$BUSCA onde $BUSCA é um parâmetro no seu script de console
- [ ] Escrever o resultado encontrado no ElasticSearch 
- [ ] Criar um Controller no Laravel para exibir a lista dos items importados 
- [ ] Na listagem, criar formulário de busca que procure no elastic a partir de uma palavra-chave
- [ ] Na listagem, criar um filtro que permita buscar por palavra-chave somente no título do item
- [ ] Ao clicar no item, exibir página de visão do item formatada e exibindo as imagens encontradas
- [ ] Criar uma view de dashboard
- [ ] Dashboard: exibir quantidade de itens
- [ ] Dashboard: exibir média dos preços
- [ ] Dashboard: exibir itens mais vendidos

Ao finalizar, envie um Merge Request para master.
=======
# 2go-test
>>>>>>> f4b1fbb257838f64de2e87075314867a6ad7b47d

